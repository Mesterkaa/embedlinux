/**
 * @file opg_3.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief 
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_3()
{
    string text = "Larsten er underviser p� AARHUS TECH";
    //Using [], i can change a single charather in a string. A string is just a char array
    text[0] = 'K';
    cout << text;
}