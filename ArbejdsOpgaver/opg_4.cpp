/**
 * @file opg_4.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief 
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_4() {
	//While (true), just keeps looping.
	while (true) {
		int input = 0;
		cout << "Input a value to find the real price: ";
		cin >> input;
		//Again by chaining << i can format the number as i go.
		cout << input * 1.25 << " kr. \n";
	}
}