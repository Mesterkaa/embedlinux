/**
 * @file opg_1.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief Opgave 1
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_1()
{
    //cout outputs the following text
    cout << "Please write your name!\n";
    string name;
    //cin takes an input and stores it in the following variable
    cin >> name;
    cout << "Please write your age!\n";
    int age;
    cin >> age;

    //Chaing << together creates one string that is printed together.
    cout << "Hello " << name << ". You are " << age << " years old.";
}