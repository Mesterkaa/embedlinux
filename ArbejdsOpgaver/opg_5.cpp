/**
 * @file opg_5.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief 
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_5() {
	while (true) {
		//First i get the users age.
		int age = 0;
		cout << "Input your age to find out what movies you can watch: ";
		cin >> age;
		string type;
		//When i figure out what range the user is in.
		//cin.fail() triggers if something could be stored in the variable. Like if the user inputs 'a', it can't store that in a int.
		if (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			//If the users inputs a faulty number, the age is set to 0.
			age = 0;
		}
		//It when gets the type text.
		if (age > 18) {
			type = "R rated";
		}
		else if (age > 13) {
			type = "PG-13";
		}
		else {
			type = "PG";
		}
		//And prints it in a sentence.
		cout << "You can watch " << type << " movies.\n";
	}
}