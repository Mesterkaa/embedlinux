/**
 * @file opg_7.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief 
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_7() {
	while (true) {
		float fare;
		int age;
		float rate;

		//First i get the ticket price.
		cout << "What is the ticket price?: ";
		cin >> fare;

		if (!cin.fail()) { //If the input didn't fail

			//Then i get the age
			cout << "What is your age?: ";
			cin >> age;

			if (!cin.fail()) { //If the input didn't fail

				//The age range when decides the rate.
				if (age < 12) {
					rate = 0;
				}
				else if (age < 24) {
					rate = 0.7;
				} 
				else if (age < 65) {
					rate = 1;
				}
				else {
					rate = 0.75;
				}
				//ticket price * rate is the total price. 
				cout << "Final total is: " << fare * rate << " kr.\n";
			}
			else {
				
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
		}
		else {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
	}
	

}