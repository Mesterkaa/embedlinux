#include <iostream>
#include <string>
using namespace std;

#include "opg_1.h";
#include "opg_2.h";
#include "opg_3.h";
#include "opg_4.h";
#include "opg_5.h";
#include "opg_6.h";
#include "opg_7.h";


int main()
{
    //By typing 1 to 7, the user can choose which work task to view.
    string n;
    cout << "Choose number: ";
    cin >> n;
    if (n == "1") {
        opg_1();
    }
    else if (n == "2") {
        opg_2();
    }
    else if (n == "3") {
        opg_3();
    }
    else if (n == "4") {
        opg_4();
    }
    else if (n == "5") {
        opg_5();
    }
    else if (n == "6") {
        opg_6();
    }
    else if (n == "7") {
        opg_7();
    }
}



