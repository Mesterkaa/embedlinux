/**
 * @file opg_2.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief 
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_2()
{
    string text = " Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
    //substr takes charateters starting from 7 and the next 5. It's when printed
    cout << text.substr(7, 5);
}