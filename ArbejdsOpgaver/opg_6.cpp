/**
 * @file opg_6.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief 
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>
#include <string>
using namespace std;

void opg_6() {
	for (int i = 1; i < 100; i++) {
		if (i % 3 == 0 && i % 5 == 0)
		{
			cout << "vaniljebudding";
		} 
		else if (i % 5 == 0)
		{
			cout << "budding";
		}
		else if (i % 3 == 0)
		{
			cout << "vanilje";
		}
		else {
			cout << i;
		}
		cout << "\n";
	}
	
}