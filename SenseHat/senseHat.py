#!/usr/bin/python
import time
from sense_hat import SenseHat

sense = SenseHat()
while True:
    print("What do you want to read?")
    print("Temperature: 't'")
    print("Humidity: 'h'")
    print("Air pressure: 'p'")
    selection = input(": ").lower()

    if selection == "t":
        temp = sense.get_temperature()
        print("The temperatur is: %s C" % temp)

    elif selection == "h":
        humidity = sense.get_humidity()
        print("The humidity is : %s %%rH" % humidity)

    elif selection == "p":
        pressure = sense.get_pressure()
        print("The air pressure is: %s Millibars" % pressure)

    else:
        print("Invalid input")
    print("\n\n")
    time.sleep(10)
    