/**
 * @file Datalogging.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief The final version of the product. Here is implementet SenseHat, Ncurses and Datalogging. Meant to run a Raspberry Pi. "make op3, to compile"
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <SenseHat.h>                           
#include <string>                               
#include <vector>                               
#include <iostream>    
#include <fstream>                         
#include <ncurses.h>      
using namespace std;

void layout_setup(void);
void sense_setup(void);
float get_data(int return_type); //0: temp, 1: humi, 2: pres
void print_data(float data, string unit);
void log_data(float temp, float humi, float pres);

//The two ncurses windows.
WINDOW * menuwin;
WINDOW * valuewin;
//Selectable menu. To add more menu items, add an entry here.
vector<string> menu = {"Temperature", "Humidity", "Air pressure"};
SenseHat sense;

int main(void) { 
    
    sense_setup(); //See function.

    //NCURSES Start
    initscr(); //Inits ncurses.
    noecho(); //What the user types is not echoed to the screen.
    cbreak(); //And ^C will stop the application.
    clear(); //Clears the screen of old text, if any.

    layout_setup(); //See function.
    int highlight = 0; //The selected menu item starts at 0.

    //After all setup, the program keeps looping.
    while (true) {
        //Writes the menu items to the window. Turning the attribute A_REVERSE on if highlightet/selected and off if not.
        for (int i = 0; i < menu.size(); i++) {
            if (i == highlight) {
                wattron(menuwin, A_REVERSE);
            }
            mvwprintw(menuwin, i + 4, 1, menu[i].c_str());
            wattroff(menuwin, A_REVERSE);
        }
        //It when wait for a user input.
        int choice;
        choice = wgetch(menuwin);
        //If the input is KEY_UP or KEY_DOWN, highlighted is changed by 1, either ++ or --
        // '\n' is ENTER.
        switch(choice) {
            case KEY_UP:
                if (highlight != 0) highlight--;
                break;
            case KEY_DOWN:
                if (highlight != menu.size() - 1) highlight++;
                break;
            case '\n': {
                mvwprintw(valuewin, 1, 1, "             ");
                wrefresh(valuewin);
                //It when use my function to collect the data, based on the selection.
                float data = get_data(highlight);

                //It when prints the data, with the right unit, again based on selection.
                switch (highlight) {
                    case 0: {
                        print_data(data, "C");
                        break;
                    }
                    case 1: {
                        print_data(data, "%");
                        break;
                    } 
                    case 2: {
                        print_data(data, "hPa");
                        break;
                    }
                    default: {
                        print_data(data, " ");
                        break;
                    }
                }
                
                break;
            }
            default:
                break;
        }
    }
    //I never really gets to this, because the only way to break out is ^C.
    //NCURSES End
    delwin(menuwin);
    endwin(); 
    return 0; 
}

/**
 * @brief Setups the main Ncurses layout
 * 
 */
void layout_setup() {
    //Creates two windows and box around them.
    menuwin = newwin(6 + menu.size(), 40, 0,0);
    valuewin = newwin(3, 40, 7 + menu.size(),0);
    box(menuwin,0,0);
    box(valuewin,0,0);

    //Allows the use of KEY_UP and KEY_DOWN
    keypad(menuwin, true);
    //Removes the cursor.
    curs_set(0);
    //Add the main text.
    mvwprintw(menuwin, 1, 1, "Choose what you want to measure!");
    mvwprintw(menuwin, 2, 1, "Use arrow keys. Press ENTER.");

    //Refreshes the screen so the layout it just created in memory is pushed to the terminal.
    refresh();
    wrefresh(menuwin);
    wrefresh(valuewin);
}

/**
 * @brief Setups the Sensehat
 * 
 */
void sense_setup() {
    //Turns of LEDS
    sense.Effacer();
    //Sets the colour to red
    sense << setcouleur(sense.ConvertirRGB565(255,0,0));
}

/**
 * @brief Get the data from the SenseHat
 * 
 * @param return_type 1 for Temperature, 2 for Humidity, 3 for Pressure
 * @return float Sensehat meassurement
 */
float get_data(int return_type) {
    //Gets all the data.
    float temp = sense.GetTemperature();
    float humi = sense.GetHumidity();
    float pres = sense.GetPressure();
    //Logs it all
    log_data(temp, humi, pres);
    //Returns the selected data.
    if (return_type == 0) {
        return temp;
    } else if (return_type == 1) {
        return humi;
    } else if (return_type == 2) {
        return pres;
    } else {
        return 0;
    }
}

/**
 * @brief Prints SenseHat data
 * 
 * @param data any float data to be shown
 * @param unit Unit to be appended af data, like %, hPa
 */
void print_data(float data, string unit) {
    //Prints to screen
    mvwprintw(valuewin, 1, 1, "%.3f %s", data, unit.c_str());
    wrefresh(valuewin);
    //Prints to SenseHat
    sense << "  " << data << " " << unit << flush;
}

/**
 * @brief Logs Temperatur, Humidity and pressure to a csv file called: "data.csv".
 * 
 * @param temp Temperatur
 * @param humi Humidity
 * @param pres Pressure
 */
void log_data(float temp, float humi, float pres) {
    //Loads the file both as input and output
    ifstream infile;
    ofstream outfile;
    infile.open("data.csv");
    outfile.open("data.csv", ios_base::app);
    //If infile is undefined, it means that the files doesn't exist.
    if (!infile) {
        //If it's a new file, put labels ontop of the fil.
        outfile << "Temperature" << "," << "Humidity" << "," << "Air_pressure" << endl;
    }
    //When write to the file, with the data.
    outfile << temp << "," << humi << "," << pres << endl;

    //And close it.  
    infile.close();
    outfile.close();
}