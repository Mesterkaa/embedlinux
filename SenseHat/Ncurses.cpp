/**
 * @file Ncurses.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief The second version of the final product. Here is implementet SenseHat and Ncurses. Meant to run a Raspberry Pi. "make opg2, to compile"
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <SenseHat.h>                           
#include <string>                               
#include <vector>                               
#include <iostream>                             
#include <ncurses.h>      
using namespace std;

int main(void) { 
    

    //NCURSES Start
    initscr();
    SenseHat sense;
    sense.Effacer(); //Clears the LED array
    noecho(); //What the user types is not echoed to the screen.
    cbreak(); //And ^C will stop the application.
    clear(); //Clears the screen of old text, if any.
    
    //Sets the LED colour to red,
    sense << setcouleur(sense.ConvertirRGB565(255,0,0));

    //Creates the two windows and boxes around whem
    WINDOW * menuwin = newwin(9, 40, 0,0);
    WINDOW * valuewin = newwin(3, 40, 10,0);
    box(menuwin,0,0);
    box(valuewin,0,0);
    //Prints the menu in memory to the terminal.
    refresh();
    wrefresh(menuwin);
    wrefresh(valuewin);
    //Allows KEY_UP and KEY_DOWN
    keypad(menuwin, true);
    //Removes the cursor.
    curs_set(0);
    //Printes text for menu.
    mvwprintw(menuwin, 1, 1, "Choose what you want to measure!");
    mvwprintw(menuwin, 2, 1, "Use arrow keys. Press ENTER.");
    
    //The 3 menu items is stored in an array, to be shown later.
    string menu[3] = {"Temperature", "Humidity", "Air pressure"};
    
    int highlight = 0;

    wrefresh(menuwin);
    wrefresh(valuewin);
    while (true) {
        //Writes the menu items to the window. Turning the attribute A_REVERSE on if highlightet/selected and off if not.
        for (int i = 0; i < 3; i++) {
            if (i == highlight) {
                wattron(menuwin, A_REVERSE);
            }
            mvwprintw(menuwin, i + 4, 1, menu[i].c_str());
            wattroff(menuwin, A_REVERSE);
        }
        //It when wait for a user input.
        int choice;
        choice = wgetch(menuwin);
        //If the input is KEY_UP or KEY_DOWN, highlighted is changed by 1, either ++ or --
        // '\n' is ENTER.
        switch(choice) {
            case KEY_UP:
                if (highlight != 0) highlight--;
                break;
            case KEY_DOWN:
                if (highlight != 2) highlight++;
                break;
            case '\n': {
                mvwprintw(valuewin, 1, 1, "             ");
                wrefresh(valuewin);
                //Based on the selection, it gets the sensor data, and first prints it to the terminal, and then to the LED array.
                switch (highlight) {

                    case 0: {
                        float temp = sense.GetTemperature();
                        mvwprintw(valuewin, 1, 1, "%.3f °C", temp);
                        wrefresh(valuewin);
                        sense << "  " << temp << " C" << flush;
                        break;
                    }
                    case 1: {
                        float humi = sense.GetHumidity();
                        mvwprintw(valuewin, 1, 1, "%.3f %s", humi, "%");
                        wrefresh(valuewin);
                        sense << "  " << humi << " %" << flush;
                        break;
                    } 
                    case 2: {
                        float pres = sense.GetPressure();
                        mvwprintw(valuewin, 1, 1, "%.3f hPa", pres);
                        wrefresh(valuewin);
                        sense << "  " << pres << " hPa" << flush;
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    //NCURSES End
    delwin(menuwin);
    endwin(); 
    return 0; 
}
