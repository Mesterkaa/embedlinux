/**
 * @file SenseHat.cpp
 * @author Anders Wolsing Kaa (awkaa@outlook.dk)
 * @brief The first version of the final product. Here is only implementet the SenseHat. Meant to run a Raspberry Pi. "make opg1" to compile
 * @version 0.1
 * @date 2021-11-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <SenseHat.h>
#include <string>
#include <iostream>
using namespace std;

int main(){

    SenseHat sensor;
    //Set red, to be the colour shown
    sensor << setcouleur(sensor.ConvertirRGB565(255,0,0));

    //Writes a simple menu
    cout << '\n';
    cout << "What do you want to measure?\n";
    cout << "Temperature: '1'\n";
    cout << "Humidity: '2'\n";
    cout << "Air pressure: '3'\n";
    cout << ": ";

    //Gets an user input.
    string select;
    cin >> select;

    
    if (select == "1") {
        //Get temperature and print it to the terminal and the LED array
        int temp = sensor.ObtenirTemperature();
        cout << "The temperatur is: " << temp << " C\n";
        sensor << "  " << temp << " C" << flush;

    } else if (select == "2") {
        //Get Humidity and print it to the terminal and the LED array
        int humi = sensor.ObtenirHumidite();
        cout << "The humidity is: " << humi << " %\n";
        sensor << "  " << humi << " %" << flush;

    } else if (select == "3") {
        //Get Pressure and print it to the terminal and the LED array
        int pres = sensor.ObtenirPression();
        cout << "The pressure is: " << pres << " hPa\n";
        sensor << "  " << pres << " hPa" << flush;
    }
    return 0;
}
